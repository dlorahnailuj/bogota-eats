/**
 * Created by jsrolon on 25-11-2016.
 */

angular.module('bogotaEats.controllers')
.filter('bogotaEats', function (FilterService) {
  return function(reviews) {
    if(reviews) {
      var atLeastOneChecked = false;
      var filteredReviews = reviews.filter(function(review) {
          var orOverCategories = false;
          angular.forEach(FilterService.filters, function(value, categoryName) {
            for(var i = 0; i < value.values.length; i++) {
              atLeastOneChecked = atLeastOneChecked || value.values[i].checked;
              if(review[categoryName] === value.values[i].name) {
                orOverCategories = orOverCategories || value.values[i].checked;
              }
            }
          });
          return orOverCategories;
        }) || reviews;
      if(!atLeastOneChecked && filteredReviews.length === 0) {
        return reviews;
      }
      return filteredReviews;
    } else {
      return reviews;
    }
  }
});
